import React, { Component } from "react";
import { connect } from "react-redux";

export class MenuComponent extends Component {
  renderButton = (burgerlayer) => {
    return (
      <div>
        <button
          className="btn btn-success"
          onClick={() => {
            this.props.tangGiamSoLuong(burgerlayer, true);
          }}
        >
          +
        </button>
        <span> </span>
        <button
          className="btn btn-danger"
          onClick={() => {
            this.props.tangGiamSoLuong(burgerlayer, false);
          }}
        >
          -
        </button>
      </div>
    );
  };
  render() {
    return (
      <div>
        <h5>Chọn thức ăn</h5>
        <table className="table">
          <thead>
            <tr>
              <th>Thức ăn</th>
              <th></th>
              <th>Giá</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Salad</th>
              <td>{this.renderButton("Salad")}</td>
              <td>10</td>
            </tr>
            <tr>
              <th scope="row">Chesse</th>
              <td>{this.renderButton("Chesse")}</td>
              <td>10</td>
            </tr>
            <tr>
              <th scope="row">Beef</th>
              <td>{this.renderButton("Beef")}</td>
              <td>10</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td></td>
              <th scope="row" className="text-right font-weight-bold">
                Tổng tiền
              </th>
              <td>30</td>
            </tr>
            <tr>
              <th colSpan="3">
                <button
                  className="btn btn-primary float-right"
                  onClick={() => {
                    this.props.thanhToan();
                  }}
                >
                  Thanh toán
                </button>
              </th>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

// const mapStateToProps = (state) => {
//   return {
//     burger: state.BurgerReducer.burger,
//   };
// };

const mapDispatchToProps = (dispatch) => {
  return {
    tangGiamSoLuong: (burgerComp, tangGiam) => {
      console.log("click", burgerComp, tangGiam);
      dispatch({
        type: "TANG_GIAM_SL",
        burgerComp,
        tangGiam,
      });
    },
    thanhToan: () => {
      dispatch({
        type: "THANH_TOAN",
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(MenuComponent);
