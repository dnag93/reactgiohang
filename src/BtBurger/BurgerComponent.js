import React, { Component } from "react";
import burgerStyle from "./cssBurger.module.css";
import { connect } from "react-redux";

export class BurgerComponent extends Component {
  renderBurger = () => {
    let burgerComponent = [];
    let burger = this.props.burgerState.burger;
    console.log("burger",burger)
    for (let i = 0; i < burger.salad; i++) {
      burgerComponent.push(<div className={burgerStyle.salad} key={"salad-"+i}></div>);
    }
    for (let i = 0; i < burger.cheese; i++) {
      burgerComponent.push(<div className={burgerStyle.cheese}  key={"cheese-"+i}></div>);
    }
    for (let i = 0; i < burger.beef; i++) {
      burgerComponent.push(<div className={burgerStyle.beef}  key={"beef-"+i}></div>);
    }
    return burgerComponent;
  };
  render() {
    return (
      <div>
        <h5 className="text-center">Cửa hàng burger cybersoft</h5>
        <div className={burgerStyle.breadTop}></div>
        <div className="text-center">Chọn thức ăn</div>
        <button
                  className="btn btn-primary float-right"
                  onClick={() => {
                    this.props.thanhToan();
                  }}
                >
                  debug
                </button>
        {this.renderBurger()}
        <div className={burgerStyle.breadBottom}></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    burgerState: state.BurgerReducer,
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    thanhToan: () => {
      dispatch({
        type: "THANH_TOAN",
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BurgerComponent);
