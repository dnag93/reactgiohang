import React, { Component } from "react";

export default class Product extends Component {
  render() {
    // ES6 bóc tách, yêu cầu tên biến mới đặt giống thuộc tính cũ
    let { sanPham } = this.props;
    return (
      <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
        <div className="container">
          <div className="card bg-light">
            <img
              className="card-img-top"
              src={sanPham.hinhAnh}
              alt="SanPham"
              style={{ maxWidth: "100%", height: 250 }}
            />
            <div className="card-body text-center">
              <h4 className="card-title text-center">{sanPham.tenSP}</h4>
              <p className="card-text">
                Giá sản phẩm : {sanPham.gia}$
              </p>
              <a href="/blank" className="btn btn-primary">
                Detail
              </a>
              <a href="/blank" className="btn btn-danger">
                Cart
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
