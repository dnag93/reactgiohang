import React, { Component } from "react";
import Product from "./Product";

export default class ProductList extends Component {
  renderSanPham = () => {
    // let mangSanPham=this.props.mangSanPham
    // Viết lại theo es6
    let { mangSanPham } = this.props;
    return mangSanPham.map((sp, index) => {
      return (
        // <div className="col-4" key={index}></div>
        // Do trong product đã tag col sẵn vì copy source nên ko cần nữa
        <Product sanPham={sp} key={index} />
      );
    });
  };

  render() {
    return (
      <div className="container">
        <h3 className="text-center">BEST SMARTPHONE</h3>
        <div className="row">{this.renderSanPham()}</div>
      </div>
    );
  }
}
