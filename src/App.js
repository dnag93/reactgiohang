import React, { Component, Fragment } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "./App.css";
import BaiTapComponent from './component/BaiTap/BaiTapComponent';
import BaiTapTruyenFunction from "./BaiTapTruyenFunction/BaiTapTruyenFunction";
import BaiTapBurger from "./BtBurger/BaiTapBurger";


function App() {
  return (
    <BrowserRouter>
      <Fragment>
        <Switch>
          <Route exact path="/" component={BaiTapBurger} />
          <Route exact path="/btcom" component={BaiTapComponent} />
          <Route exact path="/btfunc" component={BaiTapTruyenFunction} />
        </Switch>
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
// vid - B18 start
