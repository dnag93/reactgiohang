import React, { Component } from "react";
import phoneData from "../data/phoneData.json";

export default class DanhSachSanPham extends Component {
  state = {
    sanPhamChiTiet: phoneData[0],
  };

  renderSanPham = () => {
    return phoneData.map((dienThoai, index) => {
      return (
        <div className="col-4" key={index}>
          <div className="card">
            <img
              className="card-img-top"
              src={dienThoai.hinhAnh}
              alt={dienThoai.tenSP}
            />
            <div className="card-body">
              <h4 className="card-title">{dienThoai.tenSP}</h4>
              <button
                onClick={() => {
                  this.xemChiTiet(dienThoai);
                }}
                className="btn btn-success"
              >
                Xem chi tiết
              </button>
            </div>
          </div>
        </div>
      );
    });
  };

  xemChiTiet = (sanPham) => {
    this.setState({ sanPhamChiTiet: sanPham });
  };

  render() {
    let { sanPhamChiTiet } = this.state;
    return (
      <div className="container">
        <div className="row">{this.renderSanPham()}</div>
        <hr></hr>
        <div className="row">
          <div className="col-6  text-center">
            <h3>{sanPhamChiTiet.tenSP}</h3>
            <img
              src={sanPhamChiTiet.hinhAnh}
              width={250}
              height={250}
              alt={sanPhamChiTiet.tenSP}
            />
          </div>
          <div className="col-6 text-left">
            <h3>Thông số kỹ thuật</h3>
            <table className="table">
              <tr>
                <th>Màn hình</th>
                <th>{sanPhamChiTiet.manHinh}</th>
              </tr>
              <tr>
                <th>Hệ điều hành</th>
                <th>{sanPhamChiTiet.heDieuHanh}</th>
              </tr>
              <tr>
                <th>Camera trước</th>
                <th>{sanPhamChiTiet.cameraTruoc}</th>
              </tr>
              <tr>
                <th>Camera sau</th>
                <th>{sanPhamChiTiet.cameraSau}</th>
              </tr>
              <tr>
                <th>RAM</th>
                <th>{sanPhamChiTiet.ram}</th>
              </tr>
              <tr>
                <th>ROM</th>
                <th>{sanPhamChiTiet.rom}</th>
              </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
