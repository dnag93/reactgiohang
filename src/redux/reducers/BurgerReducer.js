const burgerState = {
  burger: { salad: 2, cheese: 1, beef: 2 }, // [{name:'salad',amount:1},{name:'cheese',amount:1},{name:'beef',amount:1}]
  menu: {
    salad: 10,
    cheese: 20,
    beef: 55,
  },
  total: 85,
};

// const tangGiam = (layer, tang) => {
//   if (tang) {
//     return layer++;
//   }
//   return layer--;
// };

const BurgerReducer = (state = burgerState, action) => {
  switch (action.type) {
    case "TANG_GIAM_SL": {
      const burgerUpdate = { ...state };
      let tangGiam = 0;
      action.tangGiam ? (tangGiam = 1) : (tangGiam = -1);
      switch (action.burgerComp) {
        case "Salad": {
          burgerUpdate.burger.salad += tangGiam;
          console.log("salad update", burgerUpdate.burger);
          return burgerUpdate;
        }
        default:
          break;
      }
      break;
    }
    case "THANH_TOAN": {
      console.log("Salad layers: ", state.burger.salad);
      return { ...state };
    }
    default:
      return { ...state };
  }
};

export default BurgerReducer;
